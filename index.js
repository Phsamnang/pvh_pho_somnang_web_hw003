let btn = document.getElementById('changeBtn');
let start, min, end, sMin, eMin, price;
let txtStart = document.getElementById("start");
let txtStop = document.getElementById("endTime");
let txtPrice = document.getElementById("price");
let txtMinute = document.getElementById("minute");
function startTime() {
  start = new Date();
  sMin = start.getTime();
  console.log(Math.round(4.5));
  time = start.toLocaleTimeString().split(":");
  txtStart.innerHTML = time[0] + ":" + time[1] + time[2].split(" ")[1];
  btn.innerHTML = ' <button class="relative inline-flex items-center justify-center px-10 py-4 overflow-hidden font-mono font-medium tracking-tighter text-white bg-gray-800 rounded-lg group" onclick="stopTime();">' +
    '<span class="absolute w-0 h-0 transition-all duration-500 ease-out bg-red-500 rounded-full group-hover:w-56 group-hover:h-56">' +
    '</span>' +
    ' <span class="absolute inset-0 w-full h-full -mt-1 rounded-lg opacity-30 bg-gradient-to-b from-transparent via-transparent to-gray-700"></span>' +
    '<span class="relative"><img src="./icons/stop.png" alt=""></span>' +
    '</button>';
}
function stopTime() {
  end = new Date();
  // end.setHours(14,50,0);
  eMin = end.getTime();
  //end.toLocaleTimeString();
  time = end.toLocaleTimeString().split(":");
  txtStop.innerHTML = time[0] + ":" + time[1] + time[2].split(" ")[1];
  calulatePrice(eMin, sMin);
  btn.innerHTML = '<button class="relative inline-flex items-center justify-center px-10 py-4 overflow-hidden font-mono font-medium tracking-tighter text-white bg-gray-800 rounded-lg group" onclick="clearTime();">' +
    '<span class="absolute w-0 h-0 transition-all duration-500 ease-out bg-yellow-500 rounded-full group-hover:w-56 group-hover:h-56">' +
    '</span>' +
    ' <span class="absolute inset-0 w-full h-full -mt-1 rounded-lg opacity-30 bg-gradient-to-b from-transparent via-transparent to-gray-700"></span>' +
    '<span class="relative"><img src="./icons/cleaning.png" alt=""></span>' +
    '</button>';
}

function clearTime() {
  txtStart.innerHTML = "0:00";
  txtStop.innerHTML = "0:00";
  txtPrice.innerHTML = "";
  txtMinute.innerHTML = "";
  btn.innerHTML = ' <button class="relative inline-flex items-center justify-center px-10 py-4 overflow-hidden font-mono font-medium tracking-tighter text-white bg-gray-800 rounded-lg group" onclick="startTime();">' +
    '<span class="absolute w-0 h-0 transition-all duration-500 ease-out bg-green-500 rounded-full group-hover:w-56 group-hover:h-56">' +
    '</span>' +
    ' <span class="absolute inset-0 w-full h-full -mt-1 rounded-lg opacity-30 bg-gradient-to-b from-transparent via-transparent to-gray-700"></span>' +
    '<span class="relative"><img src="./icons/power.png" alt=""></span>' +
    '</button>';
}

setInterval(function () {
  let date = Date().split(" ");
  let realTime = new Date().toLocaleTimeString();
  document.getElementById("dateTime").innerHTML = date[0] + " " + date[1] + " " + date[2] + " " + date[3] + " " + realTime;
},
  1000);

function calulatePrice(eTime, sTime) {
  min = Math.round((eTime - sTime) / 60000);
  hour = (Math.floor((min / 60)));
  overSixty = hour * 60;
  priceHour = hour * 1500;
  if (min <= 15) {
    price = priceHour + 500;
  } else if (min <= 30) {
    price = priceHour + 1000;
  } else if (min < 60) {
    price = priceHour + 1500;
  } else {
    if (min == overSixty) {
      price = priceHour;
    } else if (min <= overSixty + 15) {
      price = priceHour + 500;
    } else if (min <= overSixty + 30) {
      price = priceHour + 1000;
    } else if (min <= overSixty + 60) {
      price = priceHour + 1500;
    } else {
      price = priceHour + 500;
    }

  }
  txtMinute.innerHTML = min;
  txtPrice.innerHTML = price;
}
















